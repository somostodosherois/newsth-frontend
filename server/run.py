from flask import Flask
from views import create_interface_blueprint

debug = False;

def create_app(debug):
    app = Flask(__name__)
    interface = create_interface_blueprint(debug)
    app.register_blueprint(interface)
    return app


app = create_app(debug)

if __name__ == '__main__':
    app.run()
