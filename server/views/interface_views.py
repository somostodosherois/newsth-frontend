from flask import Blueprint, render_template


def create_interface_blueprint(debug):
    blueprint = Blueprint('interface_views', __name__)

    @blueprint.route('/', defaults={'path': ''}, methods=['GET'])
    @blueprint.route('/<path:path>', methods=['GET'])
    def serve_template(path):
        return render_template('index.html', debug=debug)

    return blueprint
