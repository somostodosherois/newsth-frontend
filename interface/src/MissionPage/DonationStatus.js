import React from 'react';
import styled from 'styled-components';
import { withTheme } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import ProgressBar from 'ProgressBar';
import DonationButton from 'DonationButton';
import { Sieve, Medal } from 'img';

const FlexRowContainer = styled.div`
display:flex;
wrap:no-wrap;
justify-content:space-around;
margin:7.5%;
align-items:center;
`;
const FlexColumnContainer = styled.div`
display:flex;
justify-content:space-around;
align-items:center;
flex-direction:column;
`;

const HeroicActs = styled.div`
display:flex;
flex-direction:column;
align-items:flex-start;
`;

const FlexPaper = styled(Paper)`
display:flex;
justify-content:space-around;
flex-direction:column;
flex-wrap:wrap;
margin-right:5%;
margin-left:5%;
margin-bottom:40px;
&&&{border-radius:5%;}
`;
const NumberCount = styled(Typography)`
&&{
font-family:AvantGarde;
  color: ${({ theme }) => (theme
    ? theme.palette.headline.heroicBlue
    : '#4dd0e1')};
};
`;

const NumberExplanation = styled(Typography)`
&&{
font-family:OpenSans;
  color: ${({ theme }) => (theme
    ? theme.palette.headline.heroicGray
    : '#abbdd3')};
};
`;


const DonateTypography = styled(Typography)`
&&&{
font-family:OpenSans;
color: ${({ theme }) => (theme
    ? theme.palette.headline.main
    : '#6c7882')};
}
`;

const toBrazilianReais = number => number.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });

const DonateNow = props => (
  <FlexColumnContainer>
    <DonateTypography
      variant="subheading"
      theme={props.theme}
    >
      Seja um heroi
    </DonateTypography>
    <DonateTypography
      variant="subheading"
      theme={props.theme}
    >
      na vida desta criança
    </DonateTypography>
    <DonationButton />
  </FlexColumnContainer>
);

const Count = props => (
  <FlexRowContainer>
    <img
      height="54"
      width="54"
      src={props.image}
    />
    <NumberCount
      variant="display2"
      theme={props.theme}
    >
      {props.bigNumber}
    </NumberCount>
    <HeroicActs>
      <NumberExplanation
        variant="headline"
        theme={props.theme}
      >
        {props.firstLine}
      </NumberExplanation>
      <NumberExplanation
        variant="headline"
        theme={props.theme}
      >
        {props.secondLine}
      </NumberExplanation>
    </HeroicActs>
  </FlexRowContainer>
);

const MissionProgress = props => (
  <FlexColumnContainer>
    <NumberCount
      variant="display1"
      theme={props.theme}
    >
      {toBrazilianReais(props.achieved)}
    </NumberCount>
    <ProgressBar
      value={props.achieved}
      max={props.total}
    />
    <NumberExplanation
      variant="subheading"
      theme={props.theme}
    >
    meta de {toBrazilianReais(props.total)}
    </NumberExplanation>
  </FlexColumnContainer>
);


const DonationStatus = props => (
  <FlexPaper {...props}>
    <Count
      bigNumber="189"
      firstLine="atos"
      secondLine="heroicos"
      image={Medal}
      theme={props.theme}
    />
    <MissionProgress
      achieved={89527.00}
      total={100000.00}
      theme={props.theme}
    />
    <Count
      bigNumber="27"
      firstLine="dias"
      secondLine="restantes"
      image={Sieve}
      theme={props.theme}
    />
    <DonateNow theme={props.theme} />
  </FlexPaper>
);

export default withTheme()(DonationStatus);
