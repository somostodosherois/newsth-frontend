import React from 'react';
import styled from 'styled-components';
import Rewards from 'Rewards';
import MissionCallToAction from './MissionCallToAction';
import DonationStatus from './DonationStatus';
import InteractionContainer from './InteractionContainer';
import InteractionBar from 'InteractionBar';
import MissionHistory from 'MissionHistory';
import MissionNews from 'MissionNews';

const AfterBleedingArea = styled.div`  
&&{
background-color:#F8FBFF;
}
`;

const MissionPage = () =>
  (
    <div>
      <MissionCallToAction />
      <DonationStatus />
      <Rewards />
      <InteractionBar />
      <MissionHistory />
    {/*     <MissionNews/>*/}
    </div>
  );
export default MissionPage;
