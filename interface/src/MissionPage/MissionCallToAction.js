import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import { withTheme } from '@material-ui/core/styles';
import ImageAvatar from 'ImageAvatar';

const FlexContainer = styled.div`
display:flex;
align-items:center;
margin: 25px;
justify-content:center;
flex-direction:column;
flex-wrap: wrap;
`;

const OpenSansTypography = styled(Typography)`
&&{
  font-family: OpenSans;
  font-size: 1rem;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.02;
  letter-spacing: normal;
  text-align: center;
  color: ${({ theme }) => (theme
  ? theme.palette.headline.main
  : '#6c7882')};
    };
}
`;

const AvantGardeTypography = styled(Typography)`
&&{
margin-top:5%;
font-family:AvantGarde;
font-size: 1.5rem;
font-weight: bold;
font-style: normal;
font-stretch: normal;
line-height: normal;
letter-spacing: normal;
text-align: center;
color: ${({ theme }) => (theme
    ? theme.palette.headline.main
    : '#6c7882')};
    };
}
`;

const MissionCallToAction = props => (
  <FlexContainer>
    <ImageAvatar
      size={props.ImageAvatarSize}
      alt={props.MissionImageAlt}
      src="/static/img/missions/childPhoto.jpg"
    />
    <AvantGardeTypography
      variant="headline"
      theme={props.theme}
    >
      {props.MissionName}
    </AvantGardeTypography>
    <OpenSansTypography
      variant="body2"
      theme={props.theme}
      gutterBottom
    >
      {props.MissionDescription}
    </OpenSansTypography>
  </FlexContainer>
);

MissionCallToAction.propTypes = {
  MissionImageAlt: PropTypes.string,
  MissionName: PropTypes.string,
  MissionDescription: PropTypes.string,
  ImageAvatarSize: PropTypes.number,
};
MissionCallToAction.defaultProps = {
  MissionImageAlt: 'Foto do nosso super-herói',
  MissionName: 'Juntos pelo Dudu',
  MissionDescription: 'Lorem ipsum dolor sit amet congue quaeque aliquando duo no an alia possit reprehendunt vel',
  ImageAvatarSize: 180,
};

export default withTheme()(MissionCallToAction);
