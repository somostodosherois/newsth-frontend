import React from 'react';
import styled from 'styled-components';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Card from '@material-ui/core/Card';

const PlaceholderCard = styled(Card)`
min-height:390px;
margin-bottom:50px;
`;

const StyledContainer = styled.div`
margin-left:2.5%;
margin-right:2.5%;
`;

class InteractionContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      section: 'About',
    };
  }

  handleChange = (event, section) => {
    this.setState({ section });
  };

  render() {
    const { section } = this.state;
    return (
      <StyledContainer>
        <PlaceholderCard />
        <AppBar
          position="sticky"
        >
          <Tabs
            value={section}
            onChange={this.handleChange}
            centered
          >
            <Tab value="About" label="Sobre" />
            <Tab value="News" label="Novidades" />
            <Tab value="Comment" label="Comentários" />
          </Tabs>
        </AppBar>

        <PlaceholderCard />
        <PlaceholderCard />
      </StyledContainer>
    );
  }
}

export default InteractionContainer;
