import medal from '../../resources/icons/medal.png';
import sieve from '../../resources/icons/sieve.png';
import supers3 from '../../resources/icons/supers_3.png';
import box from '../../resources/icons/box.png';

export const Medal = medal;
export const Sieve = sieve;
export const Supers3 = supers3;
export const Box = box;
