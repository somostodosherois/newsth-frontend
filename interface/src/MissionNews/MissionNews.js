import React from 'react';
import styled from 'styled-components';
import Paper from '@material-ui/core/Paper';

const FlexPaper = styled(Paper)`
&&{
display:flex;
flex-direction:column;
justify-content:center;
align-items:center;
border-radius:3%;
margin: 5%;
}
`;

const CommentBox = styled.div`
&&{
background: rgb(171,189,211);
background: -moz-linear-gradient(90deg, rgba(171,189,211,1) 0%, rgba(255,255,255,1) 80%);
background: -webkit-linear-gradient(90deg, rgba(171,189,211,1) 0%, rgba(255,255,255,1) 80%);
background: linear-gradient(90deg, rgba(171,189,211,1) 0%, rgba(255,255,255,1) 80%);
filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#abbdd3",endColorstr="#ffffff",GradientType=1);
display:flex;
justify-content:space-around;
align-items:center;
border-radius:25px;
}
`;

const missionNews = () => (
  <FlexPaper>
    <CommentBox>
      <p> a </p>
    </CommentBox>
    <CommentBox>
      <p> a </p>
    </CommentBox>
    <CommentBox>
      <p> a </p>
    </CommentBox>
  </FlexPaper>
);

export default missionNews;
