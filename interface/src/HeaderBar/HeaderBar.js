import React from 'react';
import styled from 'styled-components';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import MenuIcon from '@material-ui/icons/Menu';
import { withTheme } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import ImageAvatar from 'ImageAvatar';
import STHLogo from 'STHLogo';

const LightAppBar = styled(AppBar)`
&&{
background-color:${({ theme }) => (theme
    ? theme.palette.primary.light
    : 'black')};
    };
`;

const SmallLogo = styled(STHLogo)`
&&&{
height:'80px';
}
`;

const FlexToolbar = styled(Toolbar)`
&&&{
display:flex;
justify-content: space-around;
align-items:center;
}
`;

const ColoredMenuIcon = styled(MenuIcon)`
&&{
font-size: 40px;
color:${({ theme }) => (theme
    ? theme.palette.primary.dark
    : 'black')};
}
`;


const headerBar = props => (
  <LightAppBar
    position="static"
    {...props}
  >
    <FlexToolbar>
      <IconButton
        aria-label="Menu"
      >
        <ColoredMenuIcon {...props} />
      </IconButton>
      <SmallLogo />
      <ImageAvatar
        src="/static/img/users/avatar/igor.jpg"
        size={40}
      />
    </FlexToolbar>
  </LightAppBar>
);

export default withTheme()(headerBar);
