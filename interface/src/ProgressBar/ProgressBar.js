import React from 'react';
import Typography from '@material-ui/core/Typography';
import styled, { keyframes } from 'styled-components';
import { withTheme } from '@material-ui/core/styles';

const easingAnimation = keyframes`
0% {
width: 100%;
}
100% {
width: 0%;
}
`;

const Container = styled.div`
&&{
height:30px;
margin-right:7.5%;
margin-left:7.5%;
width:85%;
position: relative;
border-radius: 35px;
overflow: hidden;
background-color: #F5FAFF;
}
`;

const Progress = styled.div`
&&&{
width:${props => props.percent}%;
height:100%;
animation-direction: reverse;
background: -moz-linear-gradient(left, #4dd0e1 0%, #00E676 100%);
background: -webkit-linear-gradient(left, #4dd0e1 0%, #00E676 100%);
background: linear-gradient(to right, #4dd0e1 0%, #00E676 100%);
background-color: #F5FAFF;
}
`;

const Behind = styled.div`
&&&{
background-color: #F5FAFF;
width:100%;
height:100%;
position: absolute;
top: 0;
right: 0;
animation-name: ${easingAnimation};
animation-fill-mode: forwards;
animation-duration: 2s;
animation-timing-function: cubic-bezier(0.4, 0.0, 0.2, 1);
}
`;

const Percent = styled(Typography)`
&&{
z-index:100;
position:absolute;
text-align:center;
font-weight:bold;
width:100%;
color:white;
}
`;

const ProgressBar = (props) => {
  const percent = (props.value / props.max).toFixed(1) * 100;
  return (
    <Container>
      <Percent theme={props.theme}>
        {percent} %
      </Percent>
      <Behind />
      <Progress percent={percent} />
    </Container>
  );
};

export default withTheme()(ProgressBar);
