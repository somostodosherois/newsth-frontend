import avantGarde from '../../resources/font/ITCAvantGardePro-Bold.otf';
import openSans from '../../resources/font/OpenSansHebrew-Regular.otf';

export const AvantGarde = avantGarde;
export const OpenSans = openSans;

