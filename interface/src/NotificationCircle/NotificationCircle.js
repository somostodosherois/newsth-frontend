import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const Circle = styled.div`
&&&{
  display: inline-flex;
  align-items: center;
  justify-content: center;
  background-color: ${props => (props.color ? props.color : '#555')};
  color: white;
  width: 1em;
  height: 1em;
  font-family:OpenSans;
  font-weight:600;
  font-size:0.7rem;
  border-radius: 100%;
  vertical-align: middle;
  padding:6px;
  margin: 0 5px;
}
`;

const notificationCircle = props =>
  (props.unread > 0 ? (
    <Circle {...props}>
      {props.unread}
    </Circle>)
    : null);

notificationCircle.propTypes = {
  unread: PropTypes.number,
};

notificationCircle.defaultProps = {
  unread: 0,
};

export default notificationCircle;
