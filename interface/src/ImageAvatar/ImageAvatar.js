import React from 'react';
import styled from 'styled-components';
import { withTheme } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import PropTypes from 'prop-types';

const ShadowedAvatar = styled(Avatar)`
&&{
box-shadow: 0px 0px 12px ${({ theme }) => (theme ? theme.palette.primary.dark : 'black')};
}
`;

const ImageAvatar = props => (
  <ShadowedAvatar
    alt={props.alt}
    src={props.src}
    style={{
        width: props.size,
        height: props.size,
    }}
    {...props}
  />
);

ImageAvatar.propTypes = {
  alt: PropTypes.string,
  src: PropTypes.string.isRequired,
  size: PropTypes.number,
};

ImageAvatar.defaultProps = {
  alt: '',
  size: 40,
};

export default withTheme()(ImageAvatar);
