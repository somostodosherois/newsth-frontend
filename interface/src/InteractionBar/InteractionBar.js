import React, { PureComponent } from 'react';
import styled from 'styled-components';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import NotificationCircle from 'NotificationCircle';
import { withSlash } from 'utils';

const AvantTitle = styled(Typography)`
&&&{
font-family: 'AvantGarde';
font-weight:bold;
font-size:0.9rem;
color:#6c7882;
}
`;

const StyledAppBar = styled(AppBar)`
&&&{
background:#F5AFF;
box-shadow:0px 0px 0px 0px;
}
`;

const StyledToolbar = styled(Toolbar)`
&&&{
display:flex;
justify-content:space-around;
width:100%;
padding:0px 0px;
margin:0px;
}
`;

const Flex = styled.div`
display:flex;
justify-content:space-between;
`;

const wrapTypography = (text, notificationColor) => (
  <Flex>
    <NotificationCircle
      color={notificationColor}
      unread={10}
    />
    <AvantTitle> {text} </AvantTitle>
  </Flex>
);

const wrapSlashed = (text, notificationColor) => (
  <Flex>
    <SlashedTypography> {text} </SlashedTypography>
  </Flex>
);

const activeTypography = (active, text, notificationColor) => (
  active ?
    wrapSlashed(text, notificationColor) :
    wrapTypography(text, notificationColor));

const SlashedTypography = withSlash(AvantTitle);

class InteractionBar extends PureComponent {
  state = {
    active: 'news',
  }

  render() {
    const { active } = this.state;
    return (
      <StyledAppBar
        position="sticky"
      >
        <StyledToolbar>
          {activeTypography(active === 'about', 'Sobre', '#abbdd3')}
          {activeTypography(active === 'news', 'Novidades', '#4dd0e1')}
          {activeTypography(active === 'comments', 'Comentários', '#ee3d31')}
        </StyledToolbar>
      </StyledAppBar>);
  }
}

export default InteractionBar;
