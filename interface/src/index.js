import React from 'react';
import ReactDOM from 'react-dom';
import { injectGlobal } from 'styled-components';
import { Provider } from 'react-redux';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { AvantGarde, OpenSans } from 'fonts';
import configureStore from 'store';
import App from 'App';
import '@brainhubeu/react-carousel/lib/style.css';

const ID_CONTAINER = 'root';
const store = configureStore();


const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#FFFFFF',
      main: '#F5FAFF',
      dark: '#ACBED4;',
      contrastText: '#000',
    },
    secondary: {
      light: '#ff8c7a',
      main: '#f25a4e',
      dark: '#b92425',
      contrastText: '#000',
    },
    headline: {
      main: '#6c7882',
      heroicBlue: '#4dd0e1',
      heroicGray: '#abbdd3',
    },
  },
});

injectGlobal`
  @font-face {
        font-family: 'AvantGarde';
        src: url(${AvantGarde}) format('opentype');
        font-weight: normal;
        font-style: normal;
  }
  @font-face {
        font-family: 'OpenSans';
        src: url(${OpenSans}) format('opentype');
        font-weight: normal;
        font-style: normal;
  }

  body {
    margin: 0;
    background-color: ${theme.palette.primary.main};
    overflow-y: auto !important;
    padding-right: 0 !important;
  }
`;

ReactDOM.render(
  <Provider store={store}>
    <MuiThemeProvider theme={theme}>
      <App />
    </MuiThemeProvider>
  </Provider>,
  document.getElementById(ID_CONTAINER),
);
