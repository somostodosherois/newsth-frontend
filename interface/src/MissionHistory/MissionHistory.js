import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ImageAvatar from 'ImageAvatar';

const MediaPlaceholer = styled.div`
&&&{
background-color:#F5FAFF;
margin:5px;
height:95px;
width:95px;
}
`;

const AvantTypography = styled(Typography)`
&&{
font-family: AvantGarde;
font-weight:bold;
}
`;

const AvantGray = styled(AvantTypography)`
&&{
color:#6c7882;
}
`;

const AvantBlue = styled(AvantTypography)`
&&{
color:#4dd0e1;
}
`;

const OpenTypography = styled(Typography)`
&&{
font-family: AvantGarde;
color:#6c7882;
}
`;

const VerticalDivider = styled.div`
&&{
height:${props => (props.height ? props.height : 'auto')};
width:${props => (props.width ? props.width : '3px')};
background-color:${props => (props.slashColor ? props.slashColor : '#4dd0e1')};
margin:0 2.5%;
}
`;

const SpaceAroundContainer = styled.div`
display:flex;
align-items:flex-start;
justify-content:space-around;
width:100%;
margin:5%;
`;

const AlignSelfContainer = styled.div`
align-self:flex-start;
margin: 2.5% 7.5%;
`;

const FlexStartContainer = styled.div`
&&&{
display:flex;
align-items:flex-start;
flex-wrap: wrap;
margin:auto;
}
`;

const ColumnContainer = styled.div`
display:flex;
flex-direction:column;
`;


const FlexPaper = styled(Paper)`
&&{
display:flex;
flex-direction:column;
justify-content:center;
align-items:center;
&&&{border-radius:25px;}
margin: 0 5%;
}
`;

const MediaSession = props => (
  <AlignSelfContainer>
    <AvantBlue
      variant="subheading"
    >
      {props.sessionTitle}
    </AvantBlue>
    {props.children}
  </AlignSelfContainer>
);

const TextSession = props => (
  <AlignSelfContainer>
    <AvantBlue
      variant="subheading"
    >
      {props.sessionTitle}
    </AvantBlue>
    <OpenTypography>
      {props.sessionContent}
    </OpenTypography>
  </AlignSelfContainer>
);

const toPixel = number => `${number.toString()}px`;

const missionHistory = props => (
  <FlexPaper>
    <SpaceAroundContainer>
      <ImageAvatar
        size={props.ImageAvatarSize}
        alt={props.MissionImageAlt}
        src="/static/img/missions/childPhoto.jpg"
      />
      <VerticalDivider height={toPixel(props.ImageAvatarSize * 0.8)} />
      <ColumnContainer>
        <AvantGray
          variant="title"
        >
          {props.MissionTitle}
        </AvantGray>
        <OpenTypography
          variant="subheading"
        >
          {props.HeroName}
        </OpenTypography>
        <OpenTypography
          variant="subheading"
        >
          {props.HeroInfo}
        </OpenTypography>
      </ColumnContainer>
    </SpaceAroundContainer>
    <TextSession
      sessionTitle="história"
      sessionContent={props.History}
    />
    <TextSession
      sessionTitle="texto"
      sessionContent={props.History}
    />
    <MediaSession
      sessionTitle="fotos e vídeos"
    >
      <FlexStartContainer>
      <MediaPlaceholer />
      <MediaPlaceholer />
      <MediaPlaceholer />
      <MediaPlaceholer />
      <MediaPlaceholer />
      <MediaPlaceholer />
    </FlexStartContainer>
    </MediaSession>
  </FlexPaper>
);

missionHistory.propTypes = {
  ImageAvatarSize: PropTypes.number,
  MissionImageAlt: PropTypes.string,
  MissionTitle: PropTypes.string,
  HeroName: PropTypes.string,
  HeroInfo: PropTypes.string,
  History: PropTypes.string,
};

missionHistory.defaultProps = {
  ImageAvatarSize: 75,
  MissionImageAlt: 'Foto do nosso super-herói',
  MissionTitle: 'História da Missão',
  HeroName: 'Samuel Oliveira dos Santos',
  HeroInfo: '11 anos, São Carlos - SP',
  History: 'Lorem ipsum dolor sit amet, congue quaeque aliquando duo no, an alia possit reprehendunt vel. Per saperet accusam ad. Nec ut singulis moderatius, cum audire pertinacia instructior.',
};

export default missionHistory;
