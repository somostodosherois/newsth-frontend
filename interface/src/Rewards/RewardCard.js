import React from 'react';
import styled from 'styled-components';
import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import Collapse from '@material-ui/core/Collapse';
import Typography from '@material-ui/core/Typography';
import CardContent from '@material-ui/core/CardContent';
import { Box, Supers3 } from 'img';

const DonateButton = styled(Button)`
&&{
margin:0 3.75% 0 3.75%;
width:92.5%; height:60px;
border-radius:0 0 10px 10px;
font-size:2rem;
font-family:AvantGarde;
font-weight:bold;
color:white;
background: -moz-linear-gradient(left, #F44336 0%, #E91E63 100%);
background: -webkit-linear-gradient(left, #F44336 0%, #E91E63 100%);
background: linear-gradient(to right, #F44336 0%, #E91E63 100%);
}
`;


const StyledImg = styled.img`
width:70%;
margin: 8px 8px;
`;

const ImageContainer = styled.div`
&&&{
display:flex;
flex-direction:column;
min-height:100%;
justify-content:center;
align-items:space-between;
margin:4px 0;
}
`;

const Showcase = styled.div`
display:flex;
justify-content:center;
align-items:flex-end;
`;


const AvantGray = styled(Typography)`
&&&{
font-family: 'AvantGarde';
font-weight:bold;
color:#6c7882;
}
`;


const AvantBlue = styled(Typography)`
&&&{
font-family: 'AvantGarde';
font-weight:bold;
color:#4dd0e1;
}
`;

const BigAvantBlue = styled(AvantBlue)`
&&&{
font-size:1.25rem;
margin:0px;
line-height:1rem;
}
`;

const StyledCard = styled(Card)`
&&&{
border-radius: 9px 9px 0px 0px;
height:240px;
min-width:85%;
margin:8px 8px 0px 8px;
opacity:${({ active, index }) => (index === active ? 1 : 0.4)}
`;

const ParagraphTypography = styled(Typography)`
&&&{
font-size:0.6rem;
font-family: 'OpenSans';
font-weight:normal;
color:#abbdd3;
}
`;

const Container = styled.div`
display:flex;
flex-direction:column;
`;

const isActive = ({ activeCard, index }) => activeCard === index;

const equals = (nextProps, props) =>
  (isActive(props)
    ? nextProps.activeCard === props.index
    : nextProps.activeCard !== props.index);

class RewardCard extends React.Component {
  shouldComponentUpdate(nextProps, nextState) {
    return !equals(nextProps, this.props);
  }
  render() {
    return (
      <Container>
        <StyledCard
          active={this.props.activeCard}
          index={this.props.index}
        >
          <CardContent>
            <AvantGray>
              {this.props.titleGray}
            </AvantGray>
            <AvantBlue>
              {this.props.titleBlue}
            </AvantBlue>
            <Showcase>
              <ImageContainer>
                <StyledImg
                  src={Box}
                />
                <BigAvantBlue
                  align="center"
                >
                  {this.props.firstReward[0]}
                </BigAvantBlue>
                <AvantBlue
                  align="center"
                >
                  {this.props.firstReward[1]}
                </AvantBlue>
              </ImageContainer>
              <AvantGray
                variant="title"
              >
        +
              </AvantGray>
              <ImageContainer>
                <StyledImg
                  src={Supers3}
                />
                <BigAvantBlue
                  align="center"
                >
                  {this.props.secondReward[0]}
                </BigAvantBlue>
                <AvantBlue
                  align="center"
                >
                  {this.props.secondReward[1]}
                </AvantBlue>
              </ImageContainer>
            </Showcase>
            <ParagraphTypography>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Mauris sed suscipit metus. Suspendisse ut ligula at nisi vehicula eleifend.
            </ParagraphTypography>
          </CardContent>
        </StyledCard>
        <Collapse
          {...(isActive(this.props) ? { timeout: 400 } : {})}
          direction="down"
          in={isActive(this.props)}
          mountOnEnter
          unmountOnExit
        >
          <DonateButton >
          DOAR!
          </DonateButton>
        </Collapse>
      </Container>
    );
  }
}

export default RewardCard;
