import React from 'react';
import styled from 'styled-components';
import Typography from '@material-ui/core/Typography';
import Carousel, { Dots } from '@brainhubeu/react-carousel';
import { withSlash } from 'utils';
import RewardCard from './RewardCard';

const CarouselContainer = styled.div`
&&&{
display:flex;
flex-direction:column;
min-height:70px;
min-width:190px;
}
`;


const AvantTitle = styled(Typography)`
&&&{
font-family: 'AvantGarde';
font-weight:bold;
font-size:1.25rem;
color:#6c7882;
}
`;

const allCards = [
  {
    titleGray: 'Recompensa legal',
    titleBlue: 'R$ 10 ou mais',
    firstReward: ['1caixa', 'surpresa'],
    secondReward: ['1000', 'supers'],
  },
  {
    titleGray: 'Recompensa legal',
    titleBlue: 'R$ 20 ou mais',
    firstReward: ['1caixa', 'surpresa'],
    secondReward: ['2000', 'supers'],
  },
  {
    titleGray: 'Recompensa legal',
    titleBlue: 'R$ 30 ou mais',
    firstReward: ['2caixas', 'surpresa'],
    secondReward: ['3000', 'supers'],
  },
  {
    titleGray: 'Recompensa legal',
    titleBlue: 'R$ 50 ou mais',
    firstReward: ['2caixas', 'surpresa'],
    secondReward: ['7000', 'supers'],
  },
];

const cardToSlide = (cards, active) => cards.map((card, index) =>
  (<RewardCard
    titleGray={card.titleGray}
    titleBlue={card.titleBlue}
    firstReward={card.firstReward}
    secondReward={card.secondReward}
    index={index}
    activeCard={active}
  />));

const calculateButtonValue = (currentValue, maxLength) => (currentValue >= 0
  ? currentValue % maxLength
  : maxLength - 1);

const AvantSlash = withSlash(AvantTitle);

const MaxWidthDiv = styled.div`
&&&{
max-width:150px;
margin-left:7.5%;
}
`;

class Rewards extends React.Component {
    state = {
      value: 0,
    };
  onChange = value => this.setState({ value: calculateButtonValue(value, allCards.length) });

  render() {
    const slides = cardToSlide(allCards, this.state.value);
    return (
      <div>
        <MaxWidthDiv>
        <AvantSlash>
          Recompensas
        </AvantSlash>
      </MaxWidthDiv>
      <CarouselContainer>
        <Carousel
          value={this.state.value}
          onChange={this.onChange}
          slides={slides}
          itemWidth={220}
          clickToChange
          centered
          keepDirectionWhenDragging
        />
        <Dots value={this.state.value} onChange={this.onChange} number={allCards.length} />
      </CarouselContainer>
    </div>
    );
  }
}

export default Rewards;
