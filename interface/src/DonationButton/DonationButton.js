import React from 'react';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import { withTheme } from '@material-ui/core/styles';


const DonateButton = styled(Button)`
&&{
margin:20px 0;
width:90%;
min-height:50px;
border-radius:10px;
font-size:2rem;
font-family:AvantGarde;
font-weight:bold;
color:white;
background: -moz-linear-gradient(left, #F44336 0%, #E91E63 100%);
background: -webkit-linear-gradient(left, #F44336 0%, #E91E63 100%);
background: linear-gradient(to right, #F44336 0%, #E91E63 100%);
}
`;

const DonationButton = props => (
  <DonateButton
    color="secondary"
    variant="raised"
  >
  DOAR!
  </DonateButton>);

export default withTheme()(DonationButton);
