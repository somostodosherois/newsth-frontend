import React from 'react';
import PropTypes from 'prop-types';
import { NOT_FOUND } from 'redux-first-router';
import { connect } from 'react-redux';
import HeaderBar from 'HeaderBar';
import MissionPage from 'MissionPage';

const componentMap = {
  HOME: <div>Home</div>,
  USER: <div>Account</div>,
  MISSION: <MissionPage />,
  [NOT_FOUND]: <div>Not found</div>,
};


const App = props => (
  <div>
    <HeaderBar />
    {componentMap[props.location] || componentMap[NOT_FOUND]}
  </div>
);

App.propTypes = {
  location: PropTypes.string,
};

App.defaultProps = {
  location: 'HOME',
};


const mapStateToProps = state => ({
  location: state.location.type,
});

export default connect(mapStateToProps, null)(App);
