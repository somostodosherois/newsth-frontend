import { connectRoutes } from 'redux-first-router';
import { combineReducers, createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import createHistory from 'history/createBrowserHistory';
import queryString from 'query-string';
import routesMap from './routes';
import appReducers from './reducers';

const configureStore = (...args) => {
  const history = createHistory();
  const { middleware, enhancer, reducer } = connectRoutes(
    history,
    routesMap,
    {
      scrollTop: true,
      querySerializer: queryString,
    },
  );
  const middlewares = applyMiddleware(middleware, thunk, ...args);
  const rootReducer = combineReducers({
    ...appReducers,
    user: (state = {}) => state,
    location: reducer,
  });
  return createStore(
    rootReducer,
    composeWithDevTools(enhancer, middlewares),
  );
};

export default configureStore;
