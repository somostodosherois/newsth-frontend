const routesMap = {
  HOME: '/',
  USER: '/user/:id',
  MISSION: '/mission/:id',
};
export default routesMap;
