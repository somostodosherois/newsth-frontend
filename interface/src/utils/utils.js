import React from 'react';
import styled from 'styled-components';


const Slash = styled.div`
width:68%;
height:3px;
background-color:${props => (props.slashColor ? props.slashColor : '#4dd0e1')};
`;

const SlashContainer = styled.div`
&&&{
display:flex;
flex-direction:column;
}
`;

export const withSlash = (OldComponent, slashColor) => {
  class Slashed extends React.PureComponent {
    render() {
      return (
        <SlashContainer>
          <OldComponent {...this.props} />
          <Slash
            slashColor={slashColor}
          />
        </SlashContainer>);
    }
  }

  return Slashed;
};
