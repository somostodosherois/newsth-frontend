# README #

## What is this project? ##

This is Somos Todos Heróis frontend project, written in React, bundled in a Flask render server.

## Instalation ##

Clone this repository and make sure virtualenv is installed: 

`git clone https://your_username_here@bitbucket.org/somostodosherois/newsth-frontend.git`

`pip install virtualenv`

Then on the *server* folder create and activate a virtual envinroment and install dependencies:

`virtualenv env`

`activate env/bin/activate`

`pip install -r requirements.txt`

### Development Mode ###

Make sure that debug is set to True in run.py (TODO: make it depends on an envinroment variable);

And then run the server:

`python run.py`

On the interface folder install dependencies:

`npm install`

And run the package builder on development mode:

`npm run dev`

### Staging Mode ###

Make sure that debug is set to False in run.py (TODO: make it depends on an envinroment variable);

And then run the server:

`python run.py`


On the interface folder install dependencies:

`npm install`

And build the package:

`npm run build`

## Deploying to Heroku ##

(TODO: CI/CD Pipelinning from BitBucket to Heroku)

In server folder commit the recent changes of the bundle:

`git add .`

`git commit -m "Commit message"`

Login:

`heroku login`

Then push it to Heroku:

`git push heroku master`
